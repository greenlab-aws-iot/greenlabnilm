# GreenLabNILM #



### INTRODUCTION ###

Current U.S. electricity generation is 63%  fossil fuels (coal, petroleum, and natural gas), 20% nuclear energy, and only 18% renewable energy sources (solar, wind, hydro, geothermal, and biomass). 

All electrical appliances and devices that are on the electric grid contribute to the eventual depletion of fossil fuel sources as well as the pollution of the planet.

Renewable energy sources must contribute more to the grid by eventually supplanting fossil fuel sources.

Green energy generation should be encouraged.

There should be a way to efficiently power scientific laboratory equipment (or any electrical device) 
via a wall socket connection to a standard electrical grid to perform the following actions:

1. initiate energy conservation modes
2. access the parts of the grid that are serviced by green energy sources
3. automatically generate AI / ML models that monitor and regulate these processes


### What is GreenLabNILM? ###

**GreenLabNILM** (**N**on-**I**ntrusive **L**oad **M**onitoring) is an **IoT** solution for laboratory devices 
(microscopes, spectrometers, etc.) that automatically conserves energy consumption and taps into green renewable energy sources within an electric grid.